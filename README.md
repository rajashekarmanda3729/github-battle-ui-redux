<h1 align="center">Github battle-ui</h1>

<div align="center">
   Solution for a challenge from  <a href="https://github-battle.ui.dev/" target="_blank">github battled ui</a>.
</div>

<div align="center">
  <h3>
    <a href="https://rajashekarmandagithubbattleui.netlify.app/">
      Demo
    </a>
    <span> | </span>
    <a href="https://gitlab.com/rajashekarmanda3729/github-battle-ui-redux.git">
      Solution
    </a>
    <span> | </span>
    <a href="https://github-battle.ui.dev/">
      Challenge
    </a>
  </h3>
</div>

### UI Design 
![](src/assets/demo.png)


### Setup
* if you want to start project with vite@reactJS use below command
*         npm create vite@latest
*         npm install
*         npm run dev
*         npm run build

the above commans for server run/install depedencies and build code.

### ReactJS Functionalities
* Used react-components & ReactDOM 
* Used bootstrap for styling
* Used third party packages

### Contact

* Github [@rajashekarmanda](https://github.com/Rajashekarmanda)