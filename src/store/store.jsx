import { configureStore } from "@reduxjs/toolkit";
// import { getDefaultMiddleware } from "@reduxjs/toolkit";
import languagesSlice from "../features/languages/languagesSlice";



export const store = configureStore({
    reducer: {
        languages: languagesSlice,
    }
})