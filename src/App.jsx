import { useSelector } from "react-redux"
import Navbar from "./components/Navbar"
import DisplayUi from "./components/DisplayUi"


function App() {

  const { data, darkMode } = useSelector(store => store.languages)

  // console.log(data.items)

  return (
    <div className={`vh-100 ${darkMode && 'bg-dark'}`}>
      <Navbar />
      <DisplayUi />
    </div>
  )
}

export default App
