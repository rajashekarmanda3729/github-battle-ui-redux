import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

export const initialState = {
    data: [],
    filtersLanguages: ['All', 'JavaScript', 'Ruby', 'Java', 'CSS', 'Python'],
    isFetching: false,
    selectedFilter: 'All',
    darkMode: false
}

const url = 'https://api.github.com/search/repositories?q=stars:%3E1+language:All&sort=stars&order=desc&type=Repositories'
const jsurl = 'https://api.github.com/search/repositories?q=stars:%3E1+language:JavaScript&sort=stars&order=desc&type=Repositories'

export const getData = createAsyncThunk('languages/getData',
    async (name, thunkAPI) => {
        try {
            const response = await axios(`https://api.github.com/search/repositories?q=stars:%3E1+language:${name}&sort=stars&order=desc&type=Repositories`)
            console.log(response)
            return response.data

        } catch (error) {
            return thunkAPI.rejectWithValue('error')
            // console.log(error)
        }
    })

export const languagesSlice = createSlice({
    name: 'languages',
    initialState,
    reducers: {
        onChangeFilter: (state, { payload }) => {
            state.selectedFilter = payload
        },
        onDarkMode: (state) => {
            state.darkMode = !state.darkMode
        }
    },
    extraReducers: {
        [getData.pending]: (state) => {
            state.isFetching = true
        },
        [getData.fulfilled]: (state, action) => {
            // console.log(action.payload)
            state.isFetching = false
            state.data = action.payload
        },
        [getData.rejected]: (state) => {
            state.isFetching = false
        }
    }
})

export const { onChangeFilter,onDarkMode } = languagesSlice.actions

export default languagesSlice.reducer