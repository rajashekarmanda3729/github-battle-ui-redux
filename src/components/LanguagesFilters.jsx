import React from 'react'
import { useSelector } from 'react-redux'
import FilterBy from './FilterBy'


const LanguagesFilters = () => {

    const {filtersLanguages,darkMode} = useSelector(store => store.languages)

    // console.log(filtersLanguages)

  return (
    <section className={`${darkMode && 'bg-dark'}`}>
        {
            filtersLanguages && filtersLanguages.map(language => {
                return <FilterBy key={language} name={language}/>
            })
        }
    </section>
  )
}

export default LanguagesFilters