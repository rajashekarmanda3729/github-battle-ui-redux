import React, { useEffect } from 'react'
import { getData } from '../features/languages/languagesSlice'
import { useDispatch, useSelector } from 'react-redux'
import GithubItem from '../components/GithubItem'

const GithubItems = () => {

    const dispatch = useDispatch()
    const { selectedFilter, data, isFetching, darkMode } = useSelector(store => store.languages)

    useEffect(() => {
        dispatch(getData(selectedFilter))
    }, [selectedFilter])




    return (
        <main className={`d-flex flex-row flex-wrap justify-content-center align-items-center ${darkMode && 'bg-dark'}`}>
            {
                isFetching ?
                    <div className={`${darkMode && 'bg-dark text-light'}`}>
                        <h1 className={`m-1 mt-5 ${darkMode && 'text-light'}`}>
                            Loading
                            <span
                                className="spinner-grow spinner-grow-sm m-1"
                                role="status"
                                aria-hidden="true"
                            /><span
                                className="spinner-grow spinner-grow-sm m-1"
                                role="status"
                                aria-hidden="true"
                            /><span
                                className="spinner-grow spinner-grow-sm m-1"
                                role="status"
                                aria-hidden="true"
                            />
                        </h1>
                    </div> :
                    data.items && data.items.map(item => {
                        return <GithubItem key={item.id} itemDetails={item} index={data.items.indexOf(item)} />
                    })

            }
        </main>
    )
}

export default GithubItems