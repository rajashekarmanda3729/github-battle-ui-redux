import React from 'react'
import LanguagesFilters from './LanguagesFilters'
import GithubItems from './GithubItems'
import { useSelector } from 'react-redux'

const DisplayUi = () => {

  const { darkMode } = useSelector(store => store.languages)

  return (
    <section className={`d-flex flex-column justify-content-center align-items-center ${darkMode ? 'bg-dark' : ''}`}>
      <LanguagesFilters />
      <GithubItems />
    </section>
  )
}

export default DisplayUi