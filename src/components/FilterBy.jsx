import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { onChangeFilter } from '../features/languages/languagesSlice'

const FilterBy = ({ name }) => {

  const { selectedFilter,darkMode } = useSelector(store => store.languages)

  const dispatch = useDispatch()

  return (
    <button className={`btn border-0 ${selectedFilter === name ? "text-danger" : ''} ${selectedFilter!==name && darkMode ? 'text-light' : ''}`}
      onClick={() => dispatch(onChangeFilter(name))}>
      <h3>{name}</h3>
    </button>
  )
}

export default FilterBy