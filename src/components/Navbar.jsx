import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { onDarkMode } from '../features/languages/languagesSlice'

const Navbar = () => {

  const { darkMode } = useSelector(store => store.languages)

  const dispatch = useDispatch()

  return (
    <section className={`d-flex flex-row justify-content-between p-3 ${darkMode && 'bg-dark'}`}>
      <div className='d-flex flex-row '>
        <button className='btn text-danger'><h4 className='m-1'>Popular</h4></button>
        <button className="btn"><h4 className={`m-1 ms-3 ${darkMode && 'text-light'}`}>Battle</h4></button>
      </div>
      <div>
        <button className="btn btn-light btn-lg bg-transparent border-0 fs-2" onClick={() => dispatch(onDarkMode())}>
          {darkMode ? '💡' : '🔦'}
        </button>
      </div>
    </section>
  )
}

export default Navbar